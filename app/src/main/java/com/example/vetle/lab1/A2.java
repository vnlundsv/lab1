package com.example.vetle.lab1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class A2  extends AppCompatActivity {
    public static final int A3_STRING = 1;
    private Button B2;
    private EditText T2;
    private EditText T3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);

        //Bind GUI-elements to variables
        B2 = findViewById(R.id.B2);
        T2 = findViewById(R.id.T2);
        T3 = findViewById(R.id.T3);

        //Set fields as non-editable
        T2.setKeyListener(null);
        T3.setKeyListener(null);
        T2.setOnClickListener(null);
        T3.setOnClickListener(null);

        //Tar i mot verdi fra A1 og oppdaterer teksten i T2
        String s = getIntent().getStringExtra("T1_value");
        T2.setText("Hello " + s);

        //B2 onclick
        B2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchA3();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, A1.class);
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == A3_STRING) {
            if (resultCode == RESULT_OK) {
                System.out.print("RESULT_OK");
                String resultString = data.getStringExtra("T4_value");
                T3.setText("From A3: " + resultString);
            }
        }
    }

    private void launchA3() {
        Intent i = new Intent(this, A3.class);
        startActivityForResult(i, A3_STRING);
    }
}