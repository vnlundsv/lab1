package com.example.vetle.lab1;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class A3 extends AppCompatActivity {
    private Button B3;
    private EditText T4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);


        //Bind GUI-elements to variables
        B3 = findViewById(R.id.B3);
        T4 = findViewById(R.id.T4);

        //Button 1 onclick
        B3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchA2(T4);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, A2.class);
        setResult(Activity.RESULT_CANCELED, i);
        finish();
    }

    private void launchA2(EditText T4) {
        Intent i = new Intent(this, A2.class);
        i.putExtra("T4_value", T4.getText().toString());
        setResult(Activity.RESULT_OK, i);
        finish();
    }
}