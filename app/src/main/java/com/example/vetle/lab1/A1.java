package com.example.vetle.lab1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

public class A1 extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Button B1;
    private Spinner L1;
    private EditText T1;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);

        //Bind GUI-elements as variables
        B1 = findViewById(R.id.B1);
        L1 = findViewById(R.id.L1);
        T1 = findViewById(R.id.T1);

        //Sets up spinner with alternatives
        initL1();

        //Button 1 onclick
        B1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchA2(T1);
            }
        });

        //Listener for L1
        L1.setOnItemSelectedListener(this);
        L1.setSelection(loadSelection());
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = pref.edit();
        edit.putInt("A1Number", i);
        edit.apply();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        L1.setSelection(0);
    }

    @Override
    public void onBackPressed() {
        finish();
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
    }

    private int loadSelection() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        return pref.getInt("A1Number", 0);
    }

    private void initL1() {
        //Alternatives for spinner
        ArrayList<String> data = new ArrayList<String>();
        data.add("one");
        data.add("two");
        data.add("three");

        //Sets up adapter with content from initL1
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        L1.setAdapter(adapter);
    }

    private void launchA2(EditText T1) {
        Intent i = new Intent(this, A2.class);
        i.putExtra("T1_value", T1.getText().toString());
        startActivity(i);
    }
}
